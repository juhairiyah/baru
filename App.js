import React from "react";
import {Text, View, StyleSheet, Image} from 'react-native';

import Home from "./components/Home";

import { NavigationContainer } from "@react-navigation/native";
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Detail from "./components/Detail";

const Stack = createNativeStackNavigator();

export default function App() {
  function SplashScreen({navigation}){
    setTimeout(()=> {
      navigation.replace('Home')
    }, 5000);

    return(
      <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
        <Image source={{uri: 'https://smartandroid.fr/wp-content/uploads/2020/10/rooter-sans-pc7.png'}}
        style={{width:200,height:200}}/>
      </View>
    );
  }
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={SplashScreen} options={{headerShown:false,}}/>
        <Stack.Screen name='Home' component={Home}
        options={{
          title:'Data Karyawan',
          headerStyle:{backgroundColor:'black'},
          headerTintColor:'white',
        }}/>
        <Stack.Screen name="Detail" component={Detail}
        options={{
          title:'Detail Karyawan',
          headerStyle:{backgroundColor:'black'},
          headerTintColor:'white',
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}