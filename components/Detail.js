import React from "react";
import {Text, View, StyleSheet, Image, Button, Linking} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { Title, Card,  } from "react-native-paper";

const Detail = ({route, navigation}) => {
    const {karyawan} = route.params
    console.log(karyawan)
    return(
        <View style={{flex:1}}>
            <LinearGradient
            colors={['green','brown']}
            style={{height:'20%'}}></LinearGradient>
            <View style={{alignItems:'center'}}>
                <Image
                style={{width:120, height:120,borderRadius:60,marginTop:-50}}
                source={{uri: karyawan.foto}}/>
            </View>
            <View style={{alignItems:'center', margin:15}}>
                <Title>{karyawan.nama}</Title>
                <Text style={{fontSize:14}}>{karyawan.jabatan}</Text>
            </View>
            <Card style={{margin:3}}
            onPress={() => {
                let Txt = 'mailto:'
                let Email = karyawan.email
                Linking.openURL(Txt.concat(Email))
            }}>
            <View style={{flexDirection:'row', padding:8}}>
                <Image
                style={{width:50, height:50,borderRadius:10}}
                source={{uri:'https://cdn-icons-png.flaticon.com/512/95/95627.png'}}
                />
                <Text style={styles.teks}>{karyawan.email}</Text>
                
            </View>
            </Card>
            <Card style={{margin:3}}
            onPress={()=> {
                let Txt = 'tel:'
                let Phone = karyawan.phone
                Linking.openURL(Txt.concat(Phone))
            }}>
            <View style={{flexDirection:'row', padding:8}}>
            <Image
                style={{width:50, height:50,borderRadius:10}}
                source={{uri:'https://cdn-icons-png.flaticon.com/512/455/455705.png?w=360'}}
                />
                <Text style={styles.teks}>{karyawan.phone}</Text>
                
            </View>
            </Card>
            <Card style={{margin:3}}>
                <View style={{flexDirection:'row',padding:8}}>
                <Image
                style={{width:50, height:50,borderRadius:10}}
                source={{uri:'https://cdn-icons-png.flaticon.com/512/61/61584.png'}}
                />
                    <Text style={styles.teks}>{karyawan.gaji}</Text>
                    
                </View>
            </Card>
            <View style={{padding:10}}>
                <Button title="KEMBALI"
                onPress={() => navigation.navigate('Home')
                } color="black"/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    teks: {
        fontSize:15,
        marginTop:3,
        marginLeft:5,
        color:'black'
    }
});

export default Detail;